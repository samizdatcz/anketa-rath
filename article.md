---
title: "&bdquo;Konec lajdáctví&ldquo; i &bdquo;justice sloužící politice&ldquo;. Dvě desítky expertů rozebírají zlomový verdikt kauzy Rath"
perex: "ANKETA. Žádné soudní rozhodnutí posledních měsíců nevzbudilo tolik vášní jako zrušení odsuzujícího rozsudku nad exposlancem Davidem Rathem. Zpravodajský web Českého rozhlasu  proto zorganizoval velkou anketu mezi experty. &bdquo;Kauza ukázala, že na politiky se nesahá,&ldquo; shodují se vesměs bojovníci s korupcí. &bdquo;Je to nepřekvapivé rozhodnutí o lajdáctví při nasazování odposlechů,&ldquo; zní z druhé strany."
description: "Loni v prosinci Vrchní soud v Praze shodil odposlechy, klíčový důkaz obžaloby v kauze Davida Ratha. Bezpečnostním expertům, politologům, právníkům a dalším odborníkům jsme poslali stručnou otázku: Jaký signál vyslalo poslední rozhodnutí v kauze Rath veřejnosti, policii, justici a vůbec bojovníkům s korupcí?"
authors: ["Hana Mazancová"]
published: "30. ledna 2017"
coverimg: http://media.rozhlas.cz/_obrazek/3430615--byvaly-poslanec-a-exhejtman-stredoceskeho-kraje-david-rath--1-950x0p0.jpeg
socialimg: http://media.rozhlas.cz/_obrazek/3430615--byvaly-poslanec-a-exhejtman-stredoceskeho-kraje-david-rath--1-950x0p0.jpeg
coverimg_note: "Foto Filip Jandourek"
url: "kauza-rath"
libraries: [jquery]
styles: ["styl/anketa.css", "styl/magnific-popup.css"]
recommended:
  - link: http://www.rozhlas.cz/zpravy/domaci/_zprava/1681005
    title: Proč jsou důkazy v kauze Rath &bdquo;otrávené&ldquo;? Přečtěte si, jak soud smetl klíčové odposlechy
    perex: Rozsudek v kauze Davida Ratha smazal vrchní soud. Na vysvětlení proč aktéři případu i veřejnost museli čekat přes dva měsíce.
    image: http://media.rozhlas.cz/_obrazek/3317796--david-rath-soud--1-950x0p0.jpeg
  - link: http://www.rozhlas.cz/zpravy/domaci/_zprava/1691954
    title: Proč se Česko propadlo v korupčním žebříčku? Může za to reorganizace policie, kauza Rath i Babiš
    perex: Česká republika si v celosvětovém žebříčku vnímání korupce pohoršila o deset míst. Umístila se na 47. příčce.
    image: http://www.transparency.cz/wp-content/uploads/CPI-2015-Infografika-%C4%8Cesk%C3%A1-republika-v%C3%BDsledky.jpg
  - link: http://www.rozhlas.cz/zpravy/domaci/_zprava/1681390
    title: Unikne Rath trestu, nebo ho odsoudí i bez odposlechů? Projděte si čtyři možné scénáře
    perex: David Rath může uniknout trestu za úplatky. Stejně tak mu ale nemusí pomoct ani usnesení vrchního soudu.
    image: http://media.rozhlas.cz/_obrazek/3430616--byvaly-poslanec-a-exhejtman-stredoceskeho-kraje-david-rath--1-950x0p0.jpeg
---
 
&bdquo;Soud shledal u řízení zásadní procesní vadu, kterou je nezákonnost odposlechů a záznamů telekomunikačního provozu a sledování osob a věcí,&ldquo; stojí [ve 47stránkovém usnesení](http://www.ceska-justice.cz/wp-content/uploads/2016/12/Rath_VS_171016.pdf), na které čekal nejen zatím nepravomocně odsouzený David Rath, ale i veřejnost.
 
Senátu pražského vrchního soudu pod vedením Pavla Zelenky se totiž nelíbilo, jakým způsobem byly v celém případu nařízeny odposlechy. Okresní soud v Ústí nad Labem prý jen zkopíroval názor státního zástupce, čímž měl &bdquo;rezignovat na svou rozhodovací činnost&ldquo;.
 
Zpravodajský web Českého rozhlasu proto připravil anketu mezi bezpečnostními experty, politology, právníky a dalšími odborníky. Redakce jim poslala stručnou otázku: Jaký signál vyslalo poslední rozhodnutí v kauze Rath veřejnosti, policii, justici a vůbec bojovníkům s korupcí?
 
Někteří z oslovených rozhodnutí vítají a tvrdí, že při nasazování odposlechů se nelze řídit Machiavelliho poučkou, tedy že cíl ospravedlňuje prostředky. Jiní naopak hovoří o ráně pro víru ve spravedlivý proces. Další jsou ve svých komentářích smířlivější. Vrchní soud totiž podle nich nerozhodoval o korupci jako takové a navíc prý přesně popsal, jak nedostatek s odposlechy napravit.
 
Mimochodem i kvůli kauze Rath se Česko podle Transparency International propadlo v celosvětovém žebříčku vnímání korupce. Podle organizace si Česká republika pohoršila o deset míst a aktuálně tak zastává 47. příčku. Mezi důvody zmínila mimo jiné i nedořešení korupční kauzy Jany Nečasové (dříve Nagyové) nebo zmíněného Davida Ratha. Ostatně nad rozhodnutím vrchního soudu se &bdquo;v němém úžasu&ldquo; podivil i prezident Miloš Zeman. Pokud se u někoho najdou miliony v krabici, považuje diskuse o vině za nepatřičné. 
 
Zpravodajský web Českého rozhlasu proto oslovil více než pět desítek odborníků a expertů, aby byl v anketě zastoupen hlas všech profesních skupin české justice. Všechny došlé odpovědi zveřejňuje redakce v nezkrácené podobě v přehledném tablu.

<aside class="big">
  <div id="anketa">
   <h2 data-bso=1 style="margin-left: 25px; margin-right: 25px; margin-bottom: 0px;">Jaký signál vyslalo poslední rozhodnutí v kauze Rath veřejnosti, policii, justici&nbsp;a vůbec bojovníkům s korupcí?</h2>
   <h3 style="margin-left: 25px; margin-right: 25px; margin-bottom: 20px; margin-top: 30px; color: #606060">Kliknutím na respondenta zobrazíte jeho celou odpověď</h3>
  </div>
</aside>