$(document).ready(function() {
  $.getJSON( "data/data.json", function(data) {
    
    $(data).each(function(i) {
      $("<a class='open-popup-link' href='#popup-" + i + "'><div class='respondent'></div></a>").appendTo("#anketa");
      $(".respondent").last().append("<div class='cedulka'><strong>Klikněte a přečtěte si celou odpověď</strong></div>");
      $(".respondent").last().append("<img class='portret' src='https://samizdat.blob.core.windows.net/storage/anketa-rath/" + this.foto.slice(0, -4) + "_bw.jpg'>");
      $(".respondent").last().append("<h2 class='jmeno'>" + this.jmeno + "</h2>");
      $(".respondent").last().append("<strong>" + this.pozice + "</strong>");
      $(".respondent").last().append("<p class='veta'>&bdquo;" + this.veta + "&ldquo;</p>");
      $(".respondent").last().append("<div class='white-popup mfp-hide' id='popup-" + i + "'><strong>" + this.jmeno + ", " + this.pozice + ": </strong>&bdquo;" + this.text + "&ldquo;</div>");
    });

    $(".cedulka").hide();

    $(".respondent").hover(
      function() {
        $(this).addClass("vybrany");
        var url = $(this).find("img").attr("src").slice(0, -7) + ".jpg";
        $(this).find("img").attr("src", url);
        $(this).find(".cedulka").show();
      }, function() {
        $(this).removeClass("vybrany");
        var url = $(this).find("img").attr("src").slice(0, -4) + "_bw.jpg";
        $(this).find("img").attr("src", url);
        $(this).find(".cedulka").hide();
      }
    );
    
    $('.open-popup-link').magnificPopup({
      type:'inline',
      midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
    });

  });
});